import { ADD_TO_BUG_DETAILS } from '../constants'

const intialState = {
    bugData: ""
}

export default (state = intialState, action) => {
    console.log("bug reducer",action.data)
    switch (action.type) {
        case ADD_TO_BUG_DETAILS:
            return {
                ...state,
                bugData: action.data
            }
        default:
            return state
    }
}