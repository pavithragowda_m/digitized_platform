import { ADD_TO_WELCOME } from '../constants'

const intialState = {
    userData: ""
}

export default (state = intialState, action) => {
    console.log("user reducer",action.data)
    switch (action.type) {
        case ADD_TO_WELCOME:
            return {
                ...state,
                userData: action.data
            }
        default:
            return state
    }
}

