import { combineReducers } from 'redux'
import { persistReducer } from 'redux-persist';
import storage from 'redux-persist/lib/storage'
import userReducer from './Reducers/userReducer'
import bugReducer from './Reducers/bugReducer'

const persistConfig = {
    key: 'root',
    storage,
    whitelist: ['user','bugDetails']
  };

export const rootReducer = combineReducers({
    user: userReducer,
    bugDetails: bugReducer
})

export default persistReducer(persistConfig, rootReducer);
