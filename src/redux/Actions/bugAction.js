import { ADD_TO_BUG_DETAILS } from "../constants";

export const addToBugDetails = (data) => {
    console.log("bug action data",data)
    return {
        type: ADD_TO_BUG_DETAILS,
        data: data
    }
}
export default addToBugDetails
