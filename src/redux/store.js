import {createStore, applyMiddleware} from 'redux'
import { persistStore } from 'redux-persist';
import logger from 'redux-logger';
import rootReducer from './root-reducer'

const middlewares = [logger]

// Create store
export const store = createStore(rootReducer, applyMiddleware(...middlewares))

// Add localstorage persistor to our store
export const persistor = persistStore(store)

export default { persistor, store };
