import './result.scss'
import React, { Component } from 'react'
import './result.scss'
import { Layout } from 'antd';
import Sidebar from '../Sidebar/sidebar'
import Header from '../Header/header'
import BreadCrumb from '../BreadCrumb/breadcrumb'

import * as am4core from '@amcharts/amcharts4/core';
import * as am4charts from '@amcharts/amcharts4/charts';
import am4themes_animated from '@amcharts/amcharts4/themes/animated';

import { withRouter } from "react-router-dom";


am4core.useTheme(am4themes_animated);

export class Result extends Component {

    componentDidUpdate(prevProps, prevState) {
        if (this.props.location.state.data !== prevProps.location.state.data) {
            this.chart.data = this.props.location.state.data;
        }
    }

    componentDidMount() {
        let chart = am4core.create('chartdiv', am4charts.XYChart);

        chart.data = this.props.location.state.data;

        /* Create axes */
        let categoryAxis = chart.xAxes.push(new am4charts.CategoryAxis());
        categoryAxis.dataFields.category = 'customer';
        categoryAxis.renderer.minGridDistance = 30;
        categoryAxis.renderer.cellStartLocation = 0.2;
        categoryAxis.renderer.cellEndLocation = 0.8;

        //...

        /* Create value axis */
        let valueAxis = chart.yAxes.push(new am4charts.ValueAxis());
        valueAxis.min = 0;
        valueAxis.max = 100;
        valueAxis.strictMinMax = true;
        valueAxis.title.text = 'coverage';

        /* Create series */
        let columnSeries = chart.series.push(new am4charts.ColumnSeries());
        columnSeries.name = 'coverage';
        columnSeries.dataFields.valueY = 'coverage';
        columnSeries.dataFields.categoryX = 'customer';
        columnSeries.columns.template.width = am4core.percent(20);

        columnSeries.columns.template.tooltipText =
          '[#fff font-size: 15px]{name} in {categoryX}:\n[/][#fff font-size: 20px]{valueY}[/] [#fff]{additional}[/]';
        columnSeries.columns.template.propertyFields.fillOpacity = 'fillOpacity';
        columnSeries.columns.template.propertyFields.stroke = 'stroke';
        columnSeries.columns.template.propertyFields.strokeWidth = 'strokeWidth';
        columnSeries.columns.template.propertyFields.strokeDasharray = 'columnDash';
        columnSeries.tooltip.label.textAlign = 'middle';

        let lineSeries = chart.series.push(new am4charts.LineSeries());
        lineSeries.name = 'Expenses';
        lineSeries.dataFields.valueY = 'coverage';
        lineSeries.dataFields.categoryX = 'customer';

        lineSeries.stroke = am4core.color('#fdd400');
        lineSeries.strokeWidth = 3;
        lineSeries.propertyFields.strokeDasharray = 'lineDash';
        lineSeries.tooltip.label.textAlign = 'middle';

        let bullet = lineSeries.bullets.push(new am4charts.Bullet());
        bullet.fill = am4core.color('#fdd400'); // tooltips grab fill from parent by default
        bullet.tooltipText =
          '[#fff font-size: 15px]{name} in {categoryX}:\n[/][#fff font-size: 20px]{valueY}[/] [#fff]{additional}[/]';
        let circle = bullet.createChild(am4core.Circle);
        circle.radius = 4;
        circle.fill = am4core.color('#fff');
        circle.strokeWidth = 3;

        this.chart = chart;

    }

    componentWillUnmount() {
        if (this.chart) {
            this.chart.dispose();
        }
    }

    render() {
        console.log("location props", this.props.location.state.data)
        return (
            <div className="result_css">
                <Layout>
                    <Sidebar />
                    <Layout>
                        <Header />
                        <div className="result_content_css">
                            <div className="container-fluid">
                                <div className="row">
                                    <div className="col-sm-12">
                                        <div className="content_header">
                                            <div className="col-sm-4">
                                                <h4>Customized Run</h4>
                                            </div>
                                            <div className="col-sm-8">
                                                <BreadCrumb current={2} />
                                            </div>
                                        </div>
                                        <div className="content_body">
                                            <h4>Testcase</h4>
                                            <h6>Configure Interface</h6>
                                            <div
                                                className="chart"
                                                id="chartdiv"
                                                style={{ width: '100%' }}
                                            ></div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </Layout>
                </Layout>
            </div>
        )
    }
}

export default withRouter(Result)
