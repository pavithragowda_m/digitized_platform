import React from 'react'
import './login.scss'
import LoginForm from './loginForm'
import useWindowSize from '../../Hooks/useWindowSize/index';
import { useEffect } from "react";
import { PageHeader, Popover, Button } from 'antd';
import login_img from '../../assets/images/product2.png'
import cisco_logo from '../../assets/images/cisco_logo_white_transparent.png'

const Login = () => {
    const { width } = useWindowSize();

    useEffect(() => {
        if (width >= 992) {
            document.querySelector("body").style.overflow = "hidden";
        }

        return () => {
            document.querySelector("body").style.overflow = "auto";
        };
    }, [width]);
    return (
        <div className="login_css">
            <div className="container-fluid">
                <div className="row">
                    {width > 992 &&
                        <>

                            <div className="col-lg-6 p-0 sliderContent">
                                <div className="sliderItem">
                                    <figure className="sliderImgWrapper">
                                        <img className="sliderImg" src={login_img} alt="login image" />
                                    </figure>
                                </div>
                                {/* <div className="upperImageContent">
                                    <figure className=""
                                    >
                                        <img className="cisco_img" src={cisco_logo} alt="cisco logo" />
                                    </figure>
                                </div> */}
                                <div className="sliderItemContent">
                                    <h1 className="mb-3 text-white heading">Tommorrow Starts Here!</h1>
                                    <p className="text-white">We create solutionsbuilt on intelligent networks that solve our customer's challenges.</p>
                                </div>
                                </div>
                        </>
                    }
                    <div className="col-lg-6 p-0">
                                <div className="part_2">
                                    {/* <PageHeader
                                        extra={[
                                            <Popover placement="bottom" content="about">
                                                <Button key="2" style={{ background: "transparent", color: "#fff", border: 0, cursor: "pointer" }}>About Digitized Platform</Button>
                                            </Popover>,
                                            <Button key="1" style={{ background: "transparent", color: "#fff", border: 0, cursor: "pointer" }}>Contact Admin</Button>
                                        ]}
                                    > */}
                                        <div className="content">

                                            <div className="mainContent">
                                                <h2 className="mb-3 mb-lg-4 fnt-600">Login to CISCO's</h2>
                                                <p className="fnt-600 mb-4 fnt-mlg">Context Driven Regression (CDR)</p>
                                                <LoginForm />
                                            </div>


                                        </div>
                                    {/* </PageHeader> */}
                                </div>
                    // </div>
        //     </div>
        // </div>
        // </div>
    )
}

export default Login
