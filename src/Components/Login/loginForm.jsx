import React from 'react'
import { Form, Input, Button, Checkbox, message } from 'antd';
import { UserOutlined, LockOutlined, LoadingOutlined } from '@ant-design/icons';
import { Spin } from 'antd';
import { useEffect, useState } from 'react';
import { useHistory } from 'react-router-dom';
import { connect } from 'react-redux';
import {addToWelcome} from '../../redux/Actions/userAction'

const axios = require('axios');

const LoginForm = ({addToWelcome}) => {
    const [form] = Form.useForm();
    const history = useHistory();
    const [loginData, setLoginData] = useState();

    const onFinish = async(values) => {
        console.log("values",values)
        var req = {
          userName: values.username,
          password: values.password
        }
        let querystring = require('querystring');

        await axios({
          method: 'post',
          url: '/api/ciscoMembers/',
          data: querystring.stringify(req),
          headers: {
            'Content-Type': 'application/x-www-form-urlencoded;charset=utf-8'
        },
        })
          .then((response)=> {
            console.log("response",response.data)
            setLoginData(response.data)
            if(response.data){
              if(response.data["fullname"]){
                form.resetFields()
                addToWelcome(response.data)
                console.log("if")
                history.push('/welcome');
              }
              else{
                alert("invalid credentials")
              }
            }
          })
          .catch((error)=>{
            console.log(error);
          })
    }

    return (
        <Form
            form={form}
            name="basic"
            layout="vertical"
            initialValues={{ remember: true }}
            onFinish={onFinish}
            size={'large'}
            className="form-large"
        >
            <div className="row mt-4 mx-0">
                <div className="col-12 mb-3 p-0">
                    {/* <p className="fnt-400 fnt-msrt mb-2 fnt-sm">User name *</p> */}
                    <Form.Item
                        style={{marginTop: 25, marginBottom: 30}}
                        name="username"
                        rules={[{ required: true, message: 'Please input your Cisco ID!' },{type:"email",message: 'Please enter valid Cisco ID!'}]}
                    >
                        <Input
                            type="email"
                            placeholder="Cisco ID"
                            className="input_css"
                            bordered={false}
                            style={{background: "transparent", borderBottom: "1px solid #62628e", padding:"0 0 15px 0"}}
                        />
                    </Form.Item>
                </div>
                <div className="col-12 mb-3 p-0">
                    {/* <p className="fnt-400 fnt-msrt mb-2 fnt-sm">Password *</p> */}
                    <Form.Item
                        style={{marginBottom: 50}}
                        name="password"
                        rules={[{ required: true, message: 'Please input your Password!' }]}
                    >
                        <Input
                            type="password"
                            placeholder="Password"
                            style={{background: "transparent", borderBottom: "1px solid #62628e", padding:"0 0 15px 0"}}
                            bordered={false}
                        />
                    </Form.Item>
                </div>
                <div className="col-12 mb-2 p-0">
                    {/* <Form.Item className="mb-4">
            <div className="row m-0">
              <div className="col-6 p-0">
                <Form.Item name="remember" noStyle>
                  <Checkbox>Remember me</Checkbox>
                </Form.Item>
              </div>
              <div className="col-6 p-0">
                <Link to="/auth/forgot-password" className={style.textlink}>
                  <p>forgot password</p>
                </Link>
              </div>
            </div>
          </Form.Item> */}

                    <Form.Item className="mb-0">
                        <Button
                            type="primary"
                            htmlType="submit"
                            className="w-100 text-center"
                            style={{
                                cursor: 'pointer',
                                background: "#0096d7",
                                height: '50px',
                                borderRadius: "4px"
                            }}
                        >
                            <span>Login</span>
                        </Button>
                    </Form.Item>
                </div>
                {/* <div className="col-12 p-0"></div> */}
                {/* <div className="col-12 p-0">
          <div className="row m-0">
            <div className="col-6 p-0 mt-3">
              <p className="d-sm-flex align-items-center">
                Don’t have an account?{' '}
                <Link to="/auth/signup">
                  <span
                    className={style.textlink}
                    style={{ textDecoration: 'underline', margin: '0 0 0 5px' }}
                  >
                    sign up
                  </span>
                </Link>
              </p>
            </div>
            <div className="col-6 p-0 text-right mt-3">
              <Link to="/" className={style.textlink}>
                <p className="" href="" style={{ textDecoration: 'underline' }}>
                  skip sign in
                </p>
              </Link>
            </div>
          </div>
        </div> */}
            </div>
        </Form>
    )
}

const mapDispatchToProps = {
  addToWelcome
};
export default connect(null,mapDispatchToProps)(LoginForm)
