import React from 'react'
import './breadcrumb.scss'
import { useState } from "react";
import { Steps } from 'antd';
import { useHistory } from 'react-router-dom';

const { Step } = Steps;

const BreadCrumb = (current) => {
    const history = useHistory();
    console.log("current",current)
    const[currentStep, setCurrentStep] = useState(current.current)

    const updateCurrentStep = current => {
        console.log('onChange:', current);
        setCurrentStep(current);
        if (current == 0){
            console.log("0 nav")
            history.push({pathname: "/home"})
        }
        else if(current == 1){
            console.log("1 nav")
            history.push({pathname: "/bugdetails1"})
        }
        else{
            console.log("2 nav")
            history.push({pathname: "/results"})
        }
    };

    const renderSteps = () => {
        if(currentStep == 0){
            console.log("0")
            return <><Step title="Input Type"></Step>
          <Step title="Select Testcases" disabled></Step>
          <Step title="Result" disabled/></>
        }
        else if(currentStep == 1){
            console.log("1")
            return <><Step title="Input Type"></Step>
          <Step title="Select Testcases"></Step>
          <Step title="Result" disabled /></>
        }
        else if(currentStep == 2){ 
            console.log("2")
            return <><Step title="Input Type"></Step>
          <Step title="Select Testcases"></Step>
          <Step title="Result" /></>
        }
    }
    
    return (
        <Steps current={currentStep} onChange={updateCurrentStep} responsive={true} >
          {renderSteps()}
        </Steps>
    )
}

export default BreadCrumb
