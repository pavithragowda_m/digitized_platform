import React from 'react'
import { useEffect } from "react";
import './home.scss'
import useWindowSize from '../../Hooks/useWindowSize/index';
import { useHistory } from 'react-router-dom';
import { Layout } from 'antd';
import HomeContent from './homeContent'
import Sidebar from '../Sidebar/sidebar'
import Header from '../Header/header'

const axios = require('axios');
const $ = require('jquery');

const Home = () => {
    const { width } = useWindowSize();
    const history = useHistory();

    useEffect(() => {
        if (width >= 992) {
            document.querySelector("body").style.overflow = "hidden";
        }

        return () => {
            document.querySelector("body").style.overflow = "auto";
        };
    }, [width]);

    useEffect(() => {

        let querystring = require('querystring');
        let url = '/api/login_status/'
        axios({
            method: 'post',
            url: '/api/login_status/',
            data: querystring.stringify({ url: url }),
            headers: {
                'Content-Type': 'application/x-www-form-urlencoded;charset=utf-8',
            },
        })
            .then((response) => {
                console.log(response.data);
                if (!response.data.login_status) {
                    history.push("/")
                }
            })
            .catch((error) => {
                console.log(error);
            })
    }, [])


    return (
        <div className="home_css">
            <Layout>
                <Sidebar />
                <Layout>
                    <Header />
                    <HomeContent />
                </Layout>
            </Layout>
        </div>
    )
}

export default Home
