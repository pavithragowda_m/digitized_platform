import React from 'react'
import './home.scss'
import { useEffect, useState } from 'react';
import { Select, DatePicker, Button, message, Upload, Input } from 'antd';
import { connect } from 'react-redux';
import { addToBugDetails } from '../../redux/Actions/bugAction'
import { useHistory } from 'react-router-dom';
import BreadCrumb from '../BreadCrumb/breadcrumb'
import ReactTagInput from "@pathofdev/react-tag-input";
import { PlusOutlined, MinusOutlined, RightOutlined, LeftOutlined, DownOutlined } from '@ant-design/icons';
import { Menu, Checkbox } from 'antd';
import 'antd/dist/antd.css';

const { SubMenu } = Menu;

const dateFormat = 'YYYY/MM/DD';

const axios = require('axios');

const HomeContent = ({addToBugDetails,bugData}) => {
    const history = useHistory();
    // const [checkValue, setCheckValue] = useState(['bugid']);
    const [dupCheckValue, setDupCheckValue] = useState(['bugid']);
    const [bugItems, setBugItems] = useState([])
    const [bugFileItems, setBugFileItems] = useState([])
    const [customerSelected, setCustomerSelected] = useState(null)
    const [fromDateSelected, setFromDateSelected] = useState("")
    const [toDateSelected, setToDateSelected] = useState("")
    const [datfileinputList, setDatFileInputList] = useState([{ id: 0, file: "", testcaseName: "", customer: "" }]);
    const [inputState, setInputState] = useState(0)
    const [collapsed, setCollapsed] = useState(false)
    const [options, setOptions] = useState([
        { label: 'bugid', value: 'bugid' },
        { label: 'customer', value: 'customer' },
        { label: 'time', value: 'time' },
        { label: 'datfile', value: 'datfile'}
    ])

    // const optionsWithDisabled = [
    //     { label: 'bugid', value: 'bugid' },
    //     { label: 'customer', value: 'customer' },
    //     { label: 'time', value: 'time' },
    //     { label: 'datfile', value: 'datfile'}
    // ];

    const toggle = () => {
        setCollapsed(!collapsed)
    };

    const { Option } = Select;
    const key = 'updatable'

    const handleDatFileInputChange = (e, index) => {
        const { name, value } = e.target;
        const list = [...datfileinputList];
        list[index]["id"] = index
        list[index][name] = value;
        setDatFileInputList(list);
    };

    const handleRemoveClick = index => {
        const list = [...datfileinputList];
        list.splice(index, 1);
        setDatFileInputList(list);
    };

    const handleAddClick = () => {
        setDatFileInputList([...datfileinputList, { file: "", testcaseName: "" }]);
    };

    const props = {
        beforeUpload: file => {
            if (file.type !== 'text/csv') {
                message.error(`${file.name} is not a png file`);
            }
            const reader = new FileReader();

            reader.onload = e => {
                let item = e.target.result
                console.log(item);
                let pattern = /(CSC|csc)([a-z]{2})(\d{5})/g
                let result = item.match(pattern)
                console.log("result", result)
                // setBugFileItems(result)
                setBugItems(result)
            };
            reader.readAsText(file);

            // Prevent upload
            return false;
        },

        onChange: info => {
            console.log(info.fileList);
            if (info.fileList) {
                message.success({ content: `file uploaded successfully ${info.fileList[0]["name"]}`, key })
            }
            else {
                message.error("couldn't not upload the file")
            }
        },
    };

    const datFileProps = {
        beforeUpload: file => {
            if (file.type !== 'text/dat') {
                message.error(`${file.name} is not a png file`);
            }
            const reader = new FileReader();

            reader.onload = e => {
                let item = e.target.result
                console.log(item);
            };
            reader.readAsText(file);

            // Prevent upload
            return false;
        },

        onChange: info => {
            console.log(info.fileList);
            if (info.fileList) {
                message.success({ content: `file uploaded successfully ${info.fileList[0]["name"]}`, key })
            }
            else {
                message.error("couldn't not upload the file")
            }
        },
    }

    let allowed_list = {
        "bugid": ["customer"],
        "customer": ["bugid", "time"],
        "time": ["customer"],
        "datfile": []
    }
    const onChangeCheckbox = (origin_list) => {
        // console.log("origin_list", origin_list)
        // let check_list = JSON.parse(JSON.stringify(origin_list))
        // console.log("check_list before reverse")
        // check_list.reverse()
        // console.log("check_list after reverse", check_list)
        // let first_value = check_list[0]
        // console.log("first_value", first_value)
        // let allowed = check_list.filter(val => allowed_list[first_value].indexOf(val) != -1)
        // console.log("allowed before push", allowed)
        // allowed.push(first_value)
        // console.log("allowed after push", allowed)
        // setDupCheckValue(allowed)
        // console.log(origin_list, "\t==>\t", allowed)
        setDupCheckValue(origin_list)
        if(origin_list.includes("bugid")){
            console.log("if")
            if(origin_list.includes("customer") || origin_list.includes("time") || origin_list.includes("datfile")){
                setOptions([
                    { label: 'bugid', value: 'bugid' },
                    { label: 'customer', value: 'customer' },
                    { label: 'time', value: 'time',disabled: true },
                    { label: 'datfile', value: 'datfile', disabled: true}
                ])
            }
            else{
                setOptions([
                    { label: 'bugid', value: 'bugid' },
                    { label: 'customer', value: 'customer' },
                    { label: 'time', value: 'time',disabled: true },
                    { label: 'datfile', value: 'datfile', disabled: true}
                ])
            }
        }
        else if(origin_list.includes("customer")){
            if(origin_list.includes("bugid") || origin_list.includes("time") || origin_list.includes("datfile")){
                setOptions([
                    { label: 'bugid', value: 'bugid',disabled: true },
                    { label: 'customer', value: 'customer' },
                    { label: 'time', value: 'time' },
                    { label: 'datfile', value: 'datfile', disabled: true}
                ])
            }
            else{
                setOptions([
                    { label: 'bugid', value: 'bugid',disabled: true },
                    { label: 'customer', value: 'customer' },
                    { label: 'time', value: 'time' },
                    { label: 'datfile', value: 'datfile', disabled: true}
                ])
            }
        }
        else if(origin_list.includes("time")){
            setOptions([
                { label: 'bugid', value: 'bugid',disabled: true },
                { label: 'customer', value: 'customer',disabled: true },
                { label: 'time', value: 'time' },
                { label: 'datfile', value: 'datfile', disabled: true}
            ])
        }
        else if(origin_list.includes("datfile")){
            setOptions([
                { label: 'bugid', value: 'bugid',disabled: true },
                { label: 'customer', value: 'customer',disabled: true },
                { label: 'time', value: 'time',disabled: true },
                { label: 'datfile', value: 'datfile'}
            ])
        }
        else{
            setOptions([
                { label: 'bugid', value: 'bugid'},
                { label: 'customer', value: 'customer'},
                { label: 'time', value: 'time'},
                { label: 'datfile', value: 'datfile'}
            ])
        }
    }

    const onSelected = (value) => {
        console.log(`selected ${value}`);
        setCustomerSelected(value)
    }

    const onCustomerSelected = (e,i) => {
        const list = [...datfileinputList];
        list[i]["customer"] = e
        setDatFileInputList(list)
        // list[index][name] = value;
        // setDatFileInputList(list);
        // console.log("e i value",e,i,value)
    }

    const onFromDatePicked = (date, dateString) => {
        console.log(date, dateString);
        setFromDateSelected(dateString)
    }
    const onToDatePicked = (date, dateString) => {
        console.log(date, dateString);
        setToDateSelected(dateString)
    }

    const clearInput = () => {
        setBugItems([])
    }

    const onInputRender = () => {
        let checkValue = dupCheckValue

        // let renderDatFile = 
        let renderCustomer = <div className="mt-4 mb-4"><Select
            showSearch
            style={{ width: "50%" }}
            placeholder="Select a customer"
            optionFilterProp="children"
            onChange={onSelected}
            size="large"
            filterOption={(input, option) =>
                option.children.toLowerCase().indexOf(input.toLowerCase()) >= 0
            }
        >
            <Option value="ibm">IBM</Option>
            <Option value="terralogic">Terralogic</Option>
            <Option value="oracle">Oracle</Option>
            <Option value="tcs">Tcs</Option>
        </Select>
        </div>
        let renderDate = <div className="mt-4 mb-4">
            <DatePicker size="large" onChange={onFromDatePicked} picker="week" className="date_picker_css" format={dateFormat} placeholder="From" />
            <DatePicker size="large" onChange={onToDatePicked} picker="week" className="date_picker_css" format={dateFormat} placeholder="To" />
        </div>
        let renderBugId = <div className="mt-4 mb-4">
            <div className="display-flex align-center">
                <ReactTagInput
                    tags={bugItems}
                    placeholder="Enter BugId"
                    maxTags={10}
                    editable={true}
                    readOnly={false}
                    removeOnBackspace={true}
                    onChange={(newTags) => setBugItems(newTags)}
                    validator={(value) => {
                        let pattern = /^(CSC)([a-z]{2})(\d{5,})$/
                        let result = value.match(pattern)
                        if (result != null && result[0] == value && value.length == 10) {
                            return true
                        }
                        else
                            message.info('Entered bug id is not valid');
                    }}
                />
                <Upload
                    {...props}
                    accept=".csv"
                    showUploadList={false}>
                    <Button className="file_btn_css">Upload .csv</Button>
                </Upload>
                <Button onClick={clearInput} className="btn_css">Clear all</Button>
            </div>
        </div>
        console.log("on render checkvalue", checkValue)
        console.log("on render dupCheckValue", dupCheckValue)
        if (checkValue.includes("customer")) {
            console.log("customer")
            if (checkValue.includes("time")) {
                console.log("time")
                if (checkValue.includes("bugid")) {
                    let index = checkValue.indexOf("bugid");
                    checkValue.splice(index, 1);
                    let d = [...checkValue, "bugid"]
                    console.log("...checkValue", d, typeof (d))
                    console.log("after", checkValue, dupCheckValue)
                }
                return <>{renderCustomer}{renderDate}</>
            }
            else if (checkValue.includes("bugid")) {
                console.log("bugid")
                if (checkValue.includes("time")) {
                    let index = checkValue.indexOf("time");
                    checkValue.splice(index, 1);
                    console.log("removed", checkValue)
                }
                return <>{renderBugId}{renderCustomer}</>
            }
            else
                return renderCustomer
        }
        else if (checkValue.includes("time")) {
            console.log("time")
            return renderDate
        }
        else if (checkValue.includes("datfile")) {
            console.log("dat file")
            console.log("datfileinputList", datfileinputList)
            return datfileinputList.map((x, i) => {
                console.log("datfileinputList x", x)
                return <div className="mt-4 mb-4 display-flex">
                    <input name="file" placeholder="Paste the URL here" className="input_css" value={x.file}
                        onChange={e => handleDatFileInputChange(e, i)} />
                    <Upload
                        {...datFileProps}
                        multiple
                        accept=".dat"
                        name="file"
                        showUploadList={false}
                    // onChange={e => handleDatFileInputChange(e, i)}
                    >
                        <Button className="file_btn_css">Upload .csv</Button>
                    </Upload>
                    <input name="testcaseName" placeholder="Testcase Name" className="input_css position-left" value={x.testcaseName}
                        onChange={e => handleDatFileInputChange(e, i)} />
                    <span><Select
                        showSearch
                        style={{ position: "relative", left: "-75px", width: 170 }}
                        placeholder="Select a customer"
                        optionFilterProp="children"
                        onChange={e => onCustomerSelected(e,i)}
                        size="large"
                        filterOption={(input, option) =>
                            option.children.toLowerCase().indexOf(input.toLowerCase()) >= 0
                        }
                    >
                        <Option value="ibm">IBM</Option>
                        <Option value="terralogic">Terralogic</Option>
                        <Option value="oracle">Oracle</Option>
                    </Select></span>
                    {datfileinputList.length !== 1 && <button
                        className="mr10 border-bg-outlined-none"
                        onClick={() => handleRemoveClick(i)}><MinusOutlined className="fnt-xl rem-pos-l" /></button>}
                    {datfileinputList.length - 1 === i && <button onClick={handleAddClick} className="border-bg-outlined-none"><PlusOutlined className="fnt-xl add-pos-l" /></button>}
                </div>
            })
        }
        else {
            return renderBugId
        }

    }

    const queryData = () => {
        setInputState(1)
        setCustomerSelected(null)
        var joinBugItems = bugItems
        joinBugItems.join()
        var req = null
        if(customerSelected != null){
            req= {
                bug: joinBugItems,
                customer: customerSelected
            }
        }
        else{
            req= {
                bug: joinBugItems,
            }
        }
        // bugItems()
        let querystring = require('querystring');
        axios({
            method: 'post',
            url: '/api/get_single_bug_data/',
            headers: {
                'Content-Type': 'application/x-www-form-urlencoded;charset=utf-8'
            },
            data: querystring.stringify(req)
        })
        .then((response) => {
            console.log("response", response.data)
            // if(response.data && response.data.length >= 1){
                addToBugDetails(response.data)
                console.log("bugData",bugData)
                history.push({ pathname: '/bugdetails1', state: { data: response.data } })
            // }
        })
        .catch((error) => {
            console.log(error);
        })

        // history.push({ pathname: '/bugdetails1' })

        // let req = {
        //     a: parseInt(datfileinputList[0].file),
        //     b: parseInt(datfileinputList[0].testcaseName)
        // }
        // setDatFileInputList([{file:"",testcaseName:""}])
        //     axios({
        //         method: 'post',
        //         url: '/api/dat_file/',
        //         headers: {
        //             'Content-Type': 'application/x-www-form-urlencoded;charset=utf-8'
        //         },
        //         data: querystring.stringify(req)
        //     })
        //         .then((response) => {
        //             console.log("response", response.data)
        //         })
        //         .catch((error) => {
        //             console.log(error);
        //         })
    }
    console.log("bugitems", bugItems)

    return (
        <div className="home_content_css">
            <div className="container-fluid">
                <div className="row">
                    <div className={collapsed ? "col-11" : "col-9"}>
                        <div className="home_header">
                            <div className="mr-auto">
                                <h4>Customized Run</h4>
                            </div>
                            <div className="w-70">
                                <BreadCrumb current={inputState} />
                            </div>
                        </div>
                        <div className="home_body">
                            <h4>Select Input Type</h4>
                            <div className="mt-4 mb-4 display-flex align-center">
                                {/* <Checkbox onChange={(e) => {
                                    if (e) {
                                        onChangeCheckbox([...dupCheckValue, "bugid"])
                                    }
                                    else {
                                        dupCheckValue.pop("bugid")
                                        onChangeCheckbox(dupCheckValue)
                                    }

                                }} value="bugid" checked={dupCheckValue.includes("bugid")} /><span className="ml-2 mr-4">Bug Id</span>
                                <Checkbox onChange={(e) => {
                                    if (e) {
                                        onChangeCheckbox([...dupCheckValue, "customer"])
                                    }
                                    else {
                                        dupCheckValue.pop("customer")
                                        onChangeCheckbox(dupCheckValue)
                                    }

                                }} value="customer" checked={dupCheckValue.includes("customer")} /><span className="ml-2 mr-4">Customer Name</span>
                                <Checkbox onChange={(e) => {
                                    if (e) {
                                        onChangeCheckbox([...dupCheckValue, "time"])
                                    }
                                    else {
                                        dupCheckValue.pop("time")
                                        onChangeCheckbox(dupCheckValue)
                                    }

                                }} value="time" checked={dupCheckValue.includes("time")} /><span className="ml-2 mr-4">Time Period</span>
                                <Checkbox onChange={(e) => {
                                    if (e) {
                                        onChangeCheckbox([...dupCheckValue, "datfile"])
                                    }
                                    else {
                                        dupCheckValue.pop("datfile")
                                        onChangeCheckbox(dupCheckValue)
                                    }

                                }} value="datfile" checked={dupCheckValue.includes("datfile")} /><span className="ml-2 mr-4">Upload DAT File</span> */}
                                <Checkbox.Group
                                    options={options}
                                    defaultValue={['bugid']}
                                    onChange={onChangeCheckbox}
                                />
                            </div>
                            {onInputRender()}
                            {bugItems.length != 0 || bugFileItems.length != 0 || customerSelected != null || datfileinputList[0].file != "" || datfileinputList[0].testcaseName != "" || fromDateSelected != '' && toDateSelected != '' ?
                                <Button className="btn-css" onClick={queryData}>Next</Button>
                                : <Button className="btn-css" disabled onClick={queryData}>Next</Button>
                            }
                        </div>
                    </div>
                    <div className={collapsed ? "col-1" : "col-3"}>
                        <Button type="primary" onClick={toggle} className="toggle-menu">
                            {collapsed ? <RightOutlined style={{ color: "grey" }} /> : <LeftOutlined style={{ color: "grey" }} />}
                        </Button>
                        <Menu
                            mode="inline"
                            inlineCollapsed={collapsed}
                            style={{ height: "100%" }}
                        >
                            <div className={collapsed ? "m-2 p-1" : "m-2 p-3"}>
                                <h6>Recent Runs</h6>
                                <p className={collapsed ? "display-none" : "display-block fnt-xxs"}>Customers, components and code coverage</p>
                            </div>
                            <SubMenu key="sub1" title="IBM" icon={collapsed ? <DownOutlined className="fnt-xs" /> : null}>
                                <Menu.Item key="1">
                                    Option1
                                </Menu.Item>
                            </SubMenu>
                            <SubMenu key="sub2" title="ERICSSON" icon={collapsed ? <DownOutlined className="fnt-xs" /> : null}>
                                <Menu.Item key="2" >
                                    Option2
                                </Menu.Item>
                            </SubMenu>
                            <SubMenu key="sub3" title="SONY" icon={collapsed ? <DownOutlined className="fnt-xs" /> : null}>
                                <Menu.Item key="5">Option 5</Menu.Item>
                                <Menu.Item key="6">Option 6</Menu.Item>
                                <Menu.Item key="7">Option 7</Menu.Item>
                                <Menu.Item key="8">Option 8</Menu.Item>
                            </SubMenu>

                        </Menu>
                    </div>
                </div>
            </div>
        </div>
    )
}

const mapStateToProps = state => {
    return {
        bugData: state.bugDetails.bugData
    }
}

const mapDispatchToProps = {
    addToBugDetails
};
export default connect(mapStateToProps, mapDispatchToProps)(HomeContent)
