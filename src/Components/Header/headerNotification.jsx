import React, { useState } from 'react'
import './header.scss'
import { Dropdown, Badge, List, Menu } from 'antd';
import moment from 'moment';
import { Link } from 'react-router-dom';
import { BellOutlined } from '@ant-design/icons';
import notificationIcon from '../../assets/icons/notification.svg';
import Notification from './notification'

const HeaderNotification = () => {
    const [notificationsCount, setNotificationsCount] = useState(4);

    const list=[
        {
            title:"2/5 testcase is completed",
            date:"25th may 2021",
            time:"10:12 AM"
        },
        {
            title:"3/5 testcase is completed",
            date:"29th may 2021",
            time:"9:16 AM"
        },
        {
            title:"4/5 testcase is completed",
            date:"2nd june 2021",
            time:"11:45 AM"
        }
    ]

    const notificationsMenu = (
        <Menu className="notifications">
            <ul className="col-12 notificationListingItems">
                <List
                    itemLayout="horizontal"
                    dataSource={list}
                    renderItem={(item) => (
                        <Notification
                            title={item.title}
                            date={item.date}
                            time={item.time}
                        />
                    )}
                />
            </ul>
            <div className="col-12">
                <div className="pt-3 text-center">
                    <Link to="/notifications">
                        <p className="fnt-msrt fnt-md fnt-600 text-primary">
                            See all Notifications
                        </p>
                    </Link>
                </div>
            </div>
        </Menu>
    );

    return (
        <Dropdown
            overlay={notificationsMenu}
            placement="bottomCenter"
            trigger={['click']}
            getPopupContainer={() => document.getElementById('notificationContainer')}
        >
            <Badge count={notificationsCount} overflowCount={8}>
                <a href="#" className="head-example">
                    <img src={notificationIcon} className="actionIcons" alt="" />
                </a>
            </Badge>
        </Dropdown>
    )
}

export default HeaderNotification
