import React, { useState } from 'react'
import './header.scss'
import { connect } from 'react-redux';
import HeaderNotifications from './headerNotification'

const Header = ({ data }) => {
    const [notifications, setNotifications] = useState([]);

    console.log("user data", data)

    return (
        <header className="wrapper">
            <div className="display-flex align-items-center float-r">
                <span className="mr-4" id="notificationContainer">
                    <HeaderNotifications
                        user={data}
                        list={notifications.slice(0, 10)}
                    />
                </span>
                <span className="text-align border border-right-0 border-top-0 border-bottom-0 pl-3">
                    <h6>{data.userData.given_name}</h6>
                    <p className="m-0">{data.userData.title}</p>
                </span>
            </div>
        </header>
    )
}

const mapStateToProps = (state) => ({
    data: state.user
})
export default connect(mapStateToProps)(Header)
