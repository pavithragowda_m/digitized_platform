import React from 'react'
import { useHistory } from 'react-router-dom';
import { Divider } from 'antd'

const Notification = ({ title, date, time }) => {
    const history = useHistory();

    const handleClick = (url) => {
        history.push(url);
    };

    const renderTitle = () => {
        return (
            <h4 className="fnt-500 fnt-msrt fnt-sm txt-secondary mb-0">
                {title}
            </h4>
        );
    };


    return (
        <li
            className=""
        >
            <div className="row">
                <div className="col-12">
                    <div className="">
                        {renderTitle()}
                    </div>
                </div>
                <div className="col-12">
                    <span
                        className="txt-ltgrey fnt-500 fnt-msrt fnt-xxs ml-auto mt-3 float-r"
                    >
                        started at {time} on {date}
                    </span>
                </div>
                <div className="col-12">
                    <a onClick={() => handleClick('/bugdetails1')}>
                        <h4 className="fnt-500 fnt-msrt fnt-xs txt-secondary mt-3 float-r text-decoration-underline">
                            Select test cases {'>'}
                        </h4>
                    </a>
                </div>
                <div className="col-12 mt-1">
                    <Divider className="mb-0 mt-0" />
                </div>
            </div>
        </li>
    )
}

export default Notification
