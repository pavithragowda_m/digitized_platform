import React from 'react'
import { useLocation } from "react-router-dom";
import './welcome.scss'
import useWindowSize  from '../../Hooks/useWindowSize/index';
import { useEffect } from "react";
import { useHistory } from 'react-router-dom';
import { connect } from 'react-redux';

const axios = require('axios');

const Welcome = ({data}) => {
    console.log("props",data.userData)
    const { width } = useWindowSize();
    const history = useHistory();

    useEffect(() => {
        if (width >= 992) {
            document.querySelector("body").style.overflow = "hidden";
        }

        return () => {
            document.querySelector("body").style.overflow = "auto";
        };
    }, [width]);

    useEffect(()=> {
        
        let querystring = require('querystring');
        let url = '/api/login_status/'
        axios({
          method: 'post',
          url: '/api/login_status/',
          data: querystring.stringify({url:url}),
          headers: {
            'Content-Type': 'application/x-www-form-urlencoded;charset=utf-8',
        },
        })
        .then((response) => {
            console.log(response.data);
            if (response.data.login_status) {
                setTimeout(()=>{ history.push("/home")}, 3000);
            }
            else{
                history.push("/");
            }
        })
        .catch((error)=>{
            console.log(error);
        })
    },[])

    return (
        <div className="welcome_css slideIn" id="welcome">
            <div className="container">
                <div className="row">
                    <div className="col-sm-12">
                        <div className="wel_msg_css">
                            {data ? 
                            <>
                                <h1>Hello</h1>
                                <h1>{data.userData.given_name}</h1>
                                <p className="fnt-400 fnt-xxlg">Welcome to Cisco Digitized Platform!</p>
                            </>
                            : "No Data"}
                        </div>
                    </div>
                </div>
            </div>
        </div>
    )
}
const mapStateToProps = (state) => ({
    data: state.user
  })
export default connect(mapStateToProps)(Welcome)
