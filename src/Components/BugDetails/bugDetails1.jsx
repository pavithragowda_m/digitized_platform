import React, { useState } from 'react'
import { useLocation } from "react-router-dom";
import './bugDetails.scss'
import { connect } from 'react-redux';
import { Table } from 'antd'
import { Layout, Drawer, Button, Modal } from 'antd';
import Sidebar from '../Sidebar/sidebar'
import Header from '../Header/header'
import BreadCrumb from '../BreadCrumb/breadcrumb'
import { Link } from "react-router-dom";
import { useHistory } from 'react-router-dom';

const BugDetails1 = ({ data }) => {
    const history = useHistory();
    const location = useLocation();
    const bugData = location.state && location.state.data.length != 0 ? location.state.data : []
    console.log("bugDetails", bugData)
    const [isModalVisible, setIsModalVisible] = useState(false);
    const [visible, setVisible] = useState(false)
    const [record, setRecord] = useState()
    const [remainingDiffFiles, setRemainingDiffFiles] = useState()
    const [selectedRow, setSelectedRow] = useState([])

    const showModal = () => {
        setIsModalVisible(true);
    };

    const handleOk = () => {
        setIsModalVisible(false);
    };

    const handleCancel = () => {
        setIsModalVisible(false);
    };

    const showDrawer = () => {
        setVisible(true)
    };

    const onClose = () => {
        setVisible(false)
    };

    const rowSelection = {
        onChange: (selectedRowKeys, selectedRows) => {
            console.log(`selectedRowKeys: ${selectedRowKeys}`, 'selectedRows: ', selectedRows);
            setSelectedRow(selectedRows)
        }
    };

    const viewChart = () => {
        history.push({ pathname: '/result', state: { data: selectedRow } })
    }

    const columns = [
        {
            title: 'Bug ID',
            width: 70,
            render: (text, record) => {
                return (
                    <div>{record._source.id}</div>
                )
            }
        },
        {
            title: 'Customer',
            width: 70,
            render: (text, record) => {
                return (
                    <div>{record._source.customer}</div>
                )
            }
        },
        {
            title: 'Testcase Name',
            width: 100,
            render: (text, record) => {
                return (
                    <div>{record._source["Test-name"]}</div>
                )
            }
        },
        {
            title: 'Difference File Name',
            // dataIndex: 'Diff_file',
            // key: 'Diff_file',
            width: 100,
            // render: (Diff_file) => {
            //     if (Diff_file.length > 1) {
            //         console.log("diff", Diff_file.length)
            //         return <><div>{Diff_file[0]["file_name"]}</div><div className="text-bluelight text-decoration-underline fnt-600 cursor-pointer" onClick={showModal}>+{Diff_file.length - 1} more files</div></>
            //     }
            // }
            render: (text, record) => {
                if (record._source.Diff_file.length > 1) {
                    console.log("diff", record._source.Diff_file.length)
                    return <><div>{record._source.Diff_file[0]}</div><div className="text-bluelight text-decoration-underline fnt-600 cursor-pointer" onClick={showModal}>+{record._source.Diff_file.length - 1} more files</div></>
                }
                // <div></div>
            }
            
        },
        {
            title: 'Coverage',
            width: 70,
            render: (text, record) => {
                return (
                    <div>{record._source.coverage}</div>
                )
            }
        },
        {
            title: 'Action',
            width: 70,
            render: (text, record) => {
                return (

                    // <Link to={{ pathname: "/bugdetails2", aboutProps: { bugDetailsData: record } }}>Show more</Link>
                    <div onClick={showDrawer} className="cursor-pointer text-decoration-underline">Show more</div>
                )
            }
        },
    ]

    const dataSource = [
        {
            "Test-name": "test_compare_device_memory_snapshot",
            "Priority": "",
            "Identifier": "CSCvy07958",
            "Project": "CSC.labtrunk",
            "DE-manager": "abhajai",
            "Attribute": "-OIB gw-policy",
            "Found-during": "Feature Test",
            "Product": "cedge",
            "Opener": "visragha",
            "Original-found-during": "Feature Test",
            "Dev-escape": "",
            "Headline": "IOS utd config out of sync with confd when switching multi-tenancy to unified policy",
            "By-previous-commit-value": "No",
            "Ticket_age": {
                "age": "7",
                "opened_date": "20/04/21",
                "closed_date": "27/04/21"
            },
            "Feature": "fix",
            "Found": "func-test",
            "OS-version": "",
            "Breakage": "No",
            "Branch": "",
            "Origin": "base code",
            "Component": "iosxe-utd",
            "id": "CSCvy07958",
            "Diff_file": [{
                "file_name": "/vob/ios/sys/inspect-c3pl/insp_c3pl_param_map_parser.c"
            },
            { "file_name": "/ios/sys/mcp/mcprp/mcprp_ngfw.c" },
            { "file_name": "/vob/ios/sys/inspect-c3pl/cfg_insp_c3pl_param_map.h" },
            { "file_name": "//polaris-git.cisco.c" },
            { "file_name": "/inspect-c3pl/cfg_insp_c3pl_param_map.h" },
            { "file_name": "/inspect-c3pl/insp_c3pl_param_map_parser.c" },
            { "file_name": "/vob/ios/sys/mcp/mcprp/mcprp_ngfw.c" }
            ],
            "Status": "R",
            "Severity": "3",
            "cron_id": "id123355",
            "Fix-date": "",
            "coverage": 20,
            "customer": "Tcs"
        },
        {
            "Test-name": "test_compare_device_memory_snapshot",
            "Priority": "",
            "Identifier": "CSCvy07988",
            "Project": "CSC.labtrunk",
            "DE-manager": "abhajai",
            "Attribute": "-OIB gw-policy",
            "Found-during": "Feature Test",
            "Product": "cedge",
            "Opener": "visragha",
            "Original-found-during": "Feature Test",
            "Dev-escape": "",
            "Headline": "IOS utd config out of sync with confd when switching multi-tenancy to unified policy",
            "By-previous-commit-value": "No",
            "Ticket_age": {
                "age": "7",
                "opened_date": "20/04/21",
                "closed_date": "27/04/21"
            },
            "Feature": "fix",
            "Found": "func-test",
            "OS-version": "",
            "Breakage": "No",
            "Branch": "",
            "Origin": "base code",
            "Component": "iosxe-utd",
            "id": "CSCvy07988",
            "Diff_file": [{
                "file_name": "/vob/ios/sys/inspect-c3pl/insp_c3pl_param_map_parser.c"
            },
            { "file_name": "/ios/sys/mcp/mcprp/mcprp_ngfw.c" },
            { "file_name": "/vob/ios/sys/inspect-c3pl/cfg_insp_c3pl_param_map.h" },
            { "file_name": "//polaris-git.cisco.c" },
            { "file_name": "/inspect-c3pl/cfg_insp_c3pl_param_map.h" },
            { "file_name": "/inspect-c3pl/insp_c3pl_param_map_parser.c" },
            { "file_name": "/vob/ios/sys/mcp/mcprp/mcprp_ngfw.c" }
            ],
            "Status": "R",
            "Severity": "3",
            "cron_id": "id123355",
            "Fix-date": "",
            "coverage": 60,
            "customer": "Wipro"
        }
    ]

    const diff_files = () =>{
        return remainingDiffFiles.map((files, index) => {
    // Only do this if items have no stable IDs
        return <div key={index}>
            {files}
        </div>
        });
    }

    return (
        <div className="bug_details_css">
            <Layout>
                <Sidebar />
                <Layout>
                    <Header />
                    <div className="bug_content_css">
                        <div className="container-fluid">
                            <div className="row">
                                <div className="col-12">
                                    <div className="content_header">
                                        <div className="col-sm-4">
                                            <h4>Customized Run</h4>
                                        </div>
                                        <div className="col-sm-8">
                                            <BreadCrumb current={1} />
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div className="row">
                                <div className="col-12">
                                    <div className="content_body">
                                        <h4>Bug Details</h4>
                                        <Table
                                            rowSelection={{
                                                ...rowSelection
                                            }}
                                            columns={columns}
                                            dataSource={bugData}
                                            pagination={false}
                                            scroll={{ y: 400, x: 900 }}
                                            className={'custom-ant-table'}
                                            onRow={(record, rowIndex) => {
                                                console.log("record",record)
                                                return {
                                                    onClick: () => { console.log(record); setRecord(record._source); var data = record._source.Diff_file; var index = data.indexOf(data[0]); if (index > -1) {
                                                        data.splice(index, 1);
                                                        setRemainingDiffFiles(data)  
                                                        console.log(data)
                                                      } }
                                                };
                                            }}
                                            rowKey={record => record.id}
                                        />
                                    </div>
                                </div>
                            </div>
                            <div className="btn-container">
                                {selectedRow.length != 0 ?
                                    <Button className="btn-css" onClick={viewChart}>View Chart</Button>
                                    : <Button className="btn-css" disabled onClick={viewChart}>View Chart</Button>}
                            </div>
                        </div>
                    </div>
                    {record ?
                        <Drawer
                            title={"Details related to BugID - " + record.id}
                            placement="right"
                            // closable={false}
                            onClose={onClose}
                            visible={visible}
                            getContainer={true}
                            width="380"
                        // style={{ position: 'absolute' }}
                        >
                            <div className="row">
                                <div className="col-6">
                                    <h6>Customer</h6>
                                    <p>{record.customer}</p>
                                </div>
                                <div className="col-6">
                                    <h6>Product</h6>
                                    <p>{record.Product}</p>
                                </div>
                            </div>
                            <div className="row">
                                <div className="col-6">
                                    <h6>Components</h6>
                                    <p>{record.Component}</p>
                                </div>
                                <div className="col-6">
                                    <h6>Attribute</h6>
                                    <p>{record.Attribute}</p>
                                </div>
                            </div>
                            <div className="row">
                                <div className="col-6">
                                    <h6>Status</h6>
                                    <p>{record.Status}</p>
                                </div>
                                <div className="col-6">
                                    <h6>Software Release</h6>
                                    <p>{ }</p>
                                </div>
                            </div>
                            <div className="row">
                                <div className="col-6">
                                    <h6>Integrated Release</h6>
                                    <p>{ }</p>
                                </div>
                                <div className="col-6">
                                    <h6>Severity</h6>
                                    <p>{record.Severity}</p>
                                </div>
                            </div>
                            <div className="row">
                                <div className="col-6">
                                    <h6>Automated Test</h6>
                                    <p>{ }</p>
                                </div>
                            </div>
                        </Drawer>
                        : null
                    }
                    {record ? 
                        <Modal title="Difference file" visible={isModalVisible} onOk={handleOk} onCancel={handleCancel}>
                            {remainingDiffFiles.map((files, index) => 
                                <div key={index}>
                                    <div className="row">
                                        <div className="col-8">
                                            {files.file_name}
                                            {files}
                                        </div>
                                        <div className="col-4">
                                            {record._source.coverage}
                                        </div>
                                    </div>
                                </div>)}
                        </Modal>
                    : null}
                </Layout>
            </Layout>
        </div>

    )
}
const mapStateToProps = (state) => ({
    data: state.bugDetails
})
export default connect(mapStateToProps)(BugDetails1)

