import React from 'react'
import './sidebar.scss'
import { HomeOutlined, LogoutOutlined } from '@ant-design/icons';
import { Menu } from 'antd';
import { Layout } from 'antd';
import { useHistory } from 'react-router-dom';

const { Sider } = Layout;
const axios = require('axios');

const Sidebar = () => {
    const history = useHistory();

    const Logout = () => {
        let querystring = require('querystring');
        let url = '/api/logout/'
        axios({
            method: 'post',
            url: '/api/logout/',
            responseType: 'stream',
            headers: {
                'Content-Type': 'application/x-www-form-urlencoded;charset=utf-8'
            },
            data: querystring.stringify({
                url: url
            }),
        })
            .then((response) => {
                history.push("/")
            })
            .catch((error)=>{
                console.log("error",error);
            })
    }
    
    return (
        <Sider className="sider_css"
        breakpoint="lg"
      collapsedWidth="0"
      onBreakpoint={broken => {
        console.log(broken);
      }}
      onCollapse={(collapsed, type) => {
        console.log(collapsed, type);
      }}>
            <div className="mt-5 mb-5"></div>
            <Menu mode="inline" defaultSelectedKeys={['1']} >
                <Menu.Item key="1" icon={<HomeOutlined />}>
                    <span className="fnt-700" style={{ position: "relative", top: "4px" }}>Home</span>
                </Menu.Item>
                <Menu.Item key="2" icon={<LogoutOutlined rotate={270} />} onClick={() => Logout()}>
                    <span className="fnt-700" style={{ position: "relative", top: "4px" }}>Logout</span>
                </Menu.Item>
            </Menu>
        </Sider>
    )
}

export default Sidebar
