import logo from './logo.svg';
import './App.scss';
import {
  Route, Link, BrowserRouter as Router, HashRouter,
  Switch,
} from 'react-router-dom'
import Login from './Pages/login'
import WelcomePage from './Pages/welcome'
import HomePage from './Pages/home'
import BugDetails1 from './Pages/bugdetails1'
import ResultPage from './Pages/result'
function App() {
  return (
    <Router>
      <HashRouter>
        <Switch>
          <Route path="/" exact component={Login} />
          <Route path="/welcome" exact component={WelcomePage} />
          <Route path="/home" exact component={HomePage} />
          <Route path="/bugdetails1" exact component={BugDetails1} />
          <Route path="/result" exact component={ResultPage} />
        </Switch>
      </HashRouter>
    </Router>
  );
}

export default App;
