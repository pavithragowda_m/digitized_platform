FROM ubuntu:20.04
ENV DEBIAN_FRONTEND noninteractive

ENV http_proxy=http://proxy-wsa.esl.cisco.com:80
ENV https_proxy=http://proxy-wsa.esl.cisco.com:80
ENV no_proxy=127.0.0.1/8,*.cisco.com


RUN export http_proxy=http://proxy-wsa.esl.cisco.com:80
RUN export https_proxy=http://proxy-wsa.esl.cisco.com:80
RUN export no_proxy=127.0.0.1/8,*.cisco.com



RUN apt-get update
RUN apt-get install -y --no-install-recommends apt-utils

RUN apt-get install -y  vim curl apache2 apache2-utils psmisc
RUN apt-get -y install git python3 libapache2-mod-wsgi-py3 gcc libpq-dev  libmysqlclient-dev libssl-dev  lsof net-tools

RUN ln -s /usr/bin/python3 /usr/bin/python || true
RUN apt-get -y install python3-pip python3-dev  python3-wheel 
RUN ln -s /usr/bin/pip3 /usr/bin/pip || true
RUN pip install --upgrade pip
RUN apt-get install python3-venv -y
RUN apt-get install mysql-client -y
RUN apt-get install build-essential  -y

RUN pip install django
RUN pip uninstall wheel -y
RUN pip install wheel

RUN apt-get install wget -y
RUN apt-get install gnupg -y

RUN echo "deb http://cz.archive.ubuntu.com/ubuntu trusty main" >> /etc/apt/sources.list && apt update
RUN apt install acl -y
RUN apt install sshpass -y
RUN apt-get update && apt-get install -y sudo

RUN echo "root	ALL=(ALL:ALL) ALL" >> /etc/sudoers
RUN echo "%admin ALL=(ALL) ALL" >> /etc/sudoers
RUN echo "%sudo	ALL=(ALL:ALL) ALL" >> /etc/sudoers
RUN echo "www-data ALL=(ALL) NOPASSWD: ALL" >> /etc/sudoers
RUN echo "%www-data ALL=(ALL) NOPASSWD: ALL" >> /etc/sudoers

RUN curl -sL https://deb.nodesource.com/setup_12.x | bash -
RUN apt -y install nodejs
RUN node -v
RUN npm -v


ENV DEBIAN_FRONTEND dialog

WORKDIR /var/www


USER www-data
RUN whoami
RUN pwd

RUN sudo chown www-data:www-data -R /var/www/
RUN sudo chmod 777 -R  /var/www/

RUN ls -l  ./
RUN touch test_file_permissions
RUN ls -l  ./


ARG git_url
ARG expose_port
ARG repo_dir_name

RUN export  no_proxy=".cisco.com,127.0.0.1,localhost,192.168.0.1"
ENV no_proxy=".cisco.com,127.0.0.1,localhost,192.168.0.1"

RUN echo /var/www/${repo_dir_name}

RUN ls -l && whoami


LABEL stage=before_clone

RUN echo "hii"

RUN echo ${git_url}
RUN git clone ${git_url}
WORKDIR /var/www/${repo_dir_name}



RUN ls -l  ./
RUN ls -la  ./backend/

RUN pip3 install -r ./backend/requirements.txt

RUN pwd && ls -la
RUN npm cache clean --force -g
RUN rm -rf node_modules && rm -f package-lock.json && npm cache clean --force && npm cache verify 

RUN npm config rm proxy -g
RUN npm config rm https-proxy -g
RUN npm set strict-ssl false
RUN npm config set http-proxy http://proxy-wsa.esl.cisco.com:80/

RUN mkdir -p ./node_modules/node-sass/vendor/linux-x64-51
RUN curl -L https://github.com/sass/node-sass/releases/download/v4.5.0/linux-x64-51_binding.node -o ./node_modules/node-sass/vendor/linux-x64-51/binding.node


RUN npm install
RUN npm rebuild node-sass
RUN npm run build 


RUN ls -l && whoami

ADD  ./website-config.conf /etc/apache2/sites-available/000-default.conf
RUN cat /etc/apache2/sites-available/000-default.conf

EXPOSE 8088 7000


RUN ls -l /var/www/${repo_dir_name}/build
RUN echo "" > error.log && echo "" > access.log


RUN git config --global user.email "gubr@cisco.com"


RUN echo "---------Build complete----------"
ENV project_dir=${repo_dir_name}
RUN export project_dir=${repo_dir_name}


RUN sudo chown -R :www-data /etc/ssl/


CMD sudo chown www-data:www-data -R /var/www/$project_dir/backend/ && \
    sudo chmod 777 -R /var/www/$project_dir/backend/ && \
    ls -l && ls -l /var/www/$project_dir/backend/ && ls -l /var/www/$project_dir && ls -l /etc/ssl/ && \
    sudo a2enmod rewrite && \
    sudo a2enmod proxy && sudo a2enmod proxy_http && \
    sudo a2enmod headers && \
    sudo apache2ctl configtest && \
    sudo a2dismod mpm_prefork &&  sudo a2enmod mpm_event && \
    sudo a2enmod http2 && \
    python /var/www/$project_dir/backend/manage.py makemigrations || true && \
    python /var/www/$project_dir/backend/manage.py migrate || true && \    
    apache2ctl -M &&  apache2ctl -V &&  apache2ctl -S && \
    echo "starting web server" && \
    sudo apache2ctl -D FOREGROUND