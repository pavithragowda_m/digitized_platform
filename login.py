import requests
import base64,json
import sys


url = "https://cloudsso.cisco.com/as/token.oauth2"
# url="https://cloudsso-test.cisco.com/as/token.oauth2"

proxies = {
    "http": None,
    "https": None,
}
req_data = {    
    "grant_type": "password",
    "scope": "openid profile",
    "username":sys.argv[1],
    "password":input("Enter Cisco password: ")
}

headers = {       
    "Authorization": "Basic " + base64.b64encode("mapavith:mapavith789".encode()).decode()
    }


response = requests.post(url, data=req_data, headers=headers,proxies=proxies)
print("response",response.text)
print("headers",response.headers)

token=json.loads(response.text)["access_token"]

url="https://cloudsso.cisco.com/idp/userinfo.openid"

proxies = {
    "http": None,
    "https": None,
}
req_data = { }

headers = {
    "Authorization": "Bearer "+token,
}
response = requests.post(url, data=req_data, headers=headers,proxies=proxies)

formated_data=json.dumps(json.loads(response.text), sort_keys=False, indent=1)
print(formated_data)

