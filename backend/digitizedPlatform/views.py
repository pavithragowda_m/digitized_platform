from django.shortcuts import render
import requests
import base64,json
import sys,os
from django.http import HttpResponse
from django.views.decorators.csrf import csrf_exempt
import urllib.request
import subprocess
import time
import paramiko

# from os.path import dirname, abspath

# Create your views here.

@csrf_exempt
def dat_file(request):
    a = request.POST.get("a")
    b = request.POST.get("b")
    base_dir = os.path.dirname(os.path.dirname(os.path.abspath(__file__)))+"/dattest.py"
    print("base_dir",base_dir)
    print("1",os.path.dirname(__file__))
    print("2",os.path.dirname(os.path.dirname(__file__)))
    # ps -aux | grep datatest
    ping = subprocess.Popen(['nohup','python3', '{}'.format(base_dir),'28', '90', '&'],stdout=subprocess.PIPE,universal_newlines=True)
    # ping = subprocess.Popen(['nohup', 'python3', '{}'.format(base_dir), '28', '90', '&'],stdout=subprocess.PIPE).communicate()[0].decode('utf-8').strip()
    output = ping.stdout
    print("stdout",output)
    return HttpResponse(output, content_type="text")


@csrf_exempt
def get_bug_data(request):
    req = urllib.request.Request('http://10.195.143.161:9200/cdetsindex/_search')
    with urllib.request.urlopen(req) as response:
        responsed = response.read()
        return HttpResponse(responsed, content_type="text")
    

@csrf_exempt
def get_single_bug_data(request):
    cdets = request.POST.get("bug")
    customer = request.POST.get("customer")
    # print("bug id",cdets)
    # url = "http://10.195.143.161:9200/_search"
    # data = {"query": { "bool": {"must": [{"match": {f"{cdets}.id.keyword": cdets }}]}}}
    # payload=json.dumps(data)
    
    # headers = {
    # 'Content-Type': 'application/json'
    # }

    # response = requests.request("POST", url, headers=headers, data=payload)

    # return HttpResponse(json.dumps(json.loads(response.text)))

    host="10.195.143.111"
    user="tester"
    password="v1ptela0212"
    client = paramiko.SSHClient()
    client.set_missing_host_key_policy(paramiko.MissingHostKeyPolicy())
    client.connect(host,username=user,password=password)
    # print('(cd vtest-tools_back1/suites; python cdetsParserv.py --cdetsquery cdetsmultifield-query/{0})'.format(cdets))
    print("customer",customer)
    if customer != None:
        stdin, stdout, stderr = client.exec_command('(cd vtest-tools_back1/suites; python cdetsParserv.py --cdetsquery cdetsmultifield-query/"cdets={0} customer={1}")'.format(cdets,customer))
        sftp_client = client.open_sftp()
        try:
            remote_file = sftp_client.open('./vtest-tools_back1/suites/sample.json')   
            fileData = remote_file.read().decode('utf-8')
            print("fileData",fileData) 
            stdin, stdout, stderr = client.exec_command('(rm ./vtest-tools_back1/suites/sample.json)')
        except Exception as e:
            print(str(e))
        return HttpResponse(json.dumps(json.loads(fileData), sort_keys=False, indent=1), content_type="text/json")
    else:
        stdin, stdout, stderr = client.exec_command('(cd vtest-tools_back1/suites; python cdetsParserv.py --cdetsquery cdetsmultifield-query/{0})'.format(cdets))
    # stdin, stdout, stderr = client.exec_command('(cd vtest-tools_back1/suites; python cdetsParserv.py --cdetsquery cdetsmultifield-query/{0})'.format(cdets))
        sftp_client = client.open_sftp()
        try:
            remote_file = sftp_client.open('./vtest-tools_back1/suites/sample.json')   
            fileData = remote_file.read().decode('utf-8')
            print("fileData",fileData) 
            stdin, stdout, stderr = client.exec_command('(rm ./vtest-tools_back1/suites/sample.json)')
        except Exception as e:
            print(str(e))
        return HttpResponse(json.dumps(json.loads(fileData), sort_keys=False, indent=1), content_type="text/json")
    

    # base_dir = os.path.dirname(os.path.dirname(os.path.abspath(__file__)))+"/sample.json"
    # with open (base_dir, "r") as read_it:
    #     data = json.load(read_it)
    #     return HttpResponse(json.dumps((data), sort_keys=False, indent=1), content_type="text/json")



@csrf_exempt
def ciscoMembers(request):
    # if 'userName' in request.session:
    #     print("session")

    try:
        username = request.POST.get("userName")
        password = request.POST.get("password")
        print("request",request.POST)
        url = "https://cloudsso.cisco.com/as/token.oauth2"
        # url="https://cloudsso-test.cisco.com/as/token.oauth2"

        proxies = {
            "http": None,
            "https": None,
        }
        req_data = {    
            "grant_type": "password",
            "scope": "openid profile",
            "username":request.POST.get("userName"),
            "password":request.POST.get("password")
        }

        headers = {       
            "Authorization": "Basic " + base64.b64encode("mapavith:mapavith789".encode()).decode()
            }


        response = requests.post(url, data=req_data, headers=headers,proxies=proxies)
        
        print("response1",json.dumps(response.text))

        token=json.loads(response.text)["access_token"]
        url="https://cloudsso.cisco.com/idp/userinfo.openid"

        proxies = {
            "http": None,
            "https": None,
        }
        req_data = { }

        headers = {
            "Authorization": "Bearer "+token,
        }
        
        response = requests.post(url, data=req_data, headers=headers,proxies=proxies)

        print("response",json.dumps(json.loads(response.text), sort_keys=False, indent=1))
        d = json.loads(response.text)
        userName = d["sub"]+"@cisco.com"
        print("userName",userName)
        print("before",request.session)
        if username == userName:
            request.session['email'] = username
            request.session['testing'] = "testing"
            request.session.modified = True
            print("if",dict(request.session))

        else:
            if "email" in request.session:
                del request.session['email']
                request.session.modified = True
                print("else",request.session)

    except Exception as e:
        print(str(e))

    return HttpResponse(json.dumps(json.loads(response.text), sort_keys=False, indent=1), content_type="text/json")


@csrf_exempt
def logout(request):
    try:
        del request.session['email']
        request.session.modified = True
    except Exception as e:
        print("logout error -----",str(e))
        return HttpResponse(status=500)
    return HttpResponse(json.dumps(dict(request.session), sort_keys=False, indent=1), content_type="text/json")


@csrf_exempt
def login_status(request):
    print("login status request session",dict(request.session))
    if "email" not in request.session:
        return HttpResponse(json.dumps({"login_status": False}), content_type="text/json")
    else:
        # if "type" in request.session and request.session["type"]=="admin":
        #   return HttpResponse(json.dumps({"login_status":True,"admin":True}),content_type="text/json")
        # else:
        #   return HttpResponse(json.dumps({"login_status":True}),content_type="text/json")
        return HttpResponse(json.dumps({"login_status": True}), content_type="text/json")


