from django.conf.urls import include, url
from digitizedPlatform import views

 
urlpatterns = [ 
    url(r'^ciscoMembers/$', views.ciscoMembers),
    # url(r'^file_upload/$', views.file_upload),
    url(r'^login_status/$',views.login_status),
    url(r'^logout/$', views.logout),
    url(r'^get_bug_data/$',views.get_bug_data),
    url(r'^get_single_bug_data/$',views.get_single_bug_data),
    url(r'^dat_file/$', views.dat_file)
]